<?php

namespace Entity;

/**
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(name="post")
 */
class Post {
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    protected $id;
    
    /** @Column(length=50) 
     * @var string
     */
    protected $title;

    /** @Column(length=50)
     * @var string
     */
    protected $content;

    /** @Column(type="datetime", name="added_at") */
    protected $addedAt;

    /**
     * @return mixed
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * @param mixed $addedAt
     */
    public function setAddedAt($addedAt)
    {
        $this->addedAt = $addedAt;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'addedAt' => $this->getAddedAt()
        );
    }
    
    /**
     *  @PrePersist
     */
    public function beforeSave()
    {
        $this->addedAt = new \DateTime("now");
    }
    
}