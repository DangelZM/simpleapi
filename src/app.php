<?php

require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

$app = new Silex\Application();

if (in_array(APPLICATION_ENV, array('development'))) {
    $app['debug'] = true;
}

require __DIR__ . '/registers.php';
require __DIR__ . '/routes.php';

$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
});

$app->error(function (\Exception $e, $code) use($app) {
    $error = array(
        'status' => 'error',
        'code' => $code,
        'message' => $e->getMessage()
    );

    return $app->json($error, $code);
});

return $app;