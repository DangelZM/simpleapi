<?php
namespace Controller;

use Silex\Application;
use Silex\Route;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;

class Post implements ControllerProviderInterface
{
    protected $em;

    public function connect(Application $app)
    {
        $controller = new ControllerCollection(new Route());
        $this->em = $app['orm.em'];

        $controller->post('/', function(Request $request) use ($app) {
            $response = array();

            $post = new \Entity\Post();
            $post->setTitle($request->get('title'));
            $post->setContent($request->get('content'));

            $this->em->persist($post);
            $this->em->flush();

            $response['status'] = 'ok';
            $response['addedAt'] = $post->getAddedAt();

            return $app->json($response);
        });

        $controller->get('/', function() use ($app) {
            $response = array();

            $posts = $this->em->getRepository('Entity\Post')->findAll();

            $response['status'] = 'ok';
            foreach($posts as $post) {
                $response['posts'][] = $post->toArray();
            }

            return $app->json($response);
        });

        $controller->get('/{id}', function($id) use ($app) {
            $response = array();

            $post = $this->em->getRepository('Entity\Post')->find($id);

            if($post){
                $response['status'] = "ok";
                $response['post'] = $post->toArray();
            } else {
                $app->abort(404, 'Post not found');
            }

            return $app->json($response);
        });

        return $controller;
    }

}