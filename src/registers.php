<?php

use Symfony\Component\Yaml\Parser;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Provider\DoctrineServiceProvider;


$app['config'] = $app->share(function () {
    $yaml = new Parser();
    return $yaml->parse(file_get_contents(__DIR__ . '/../app/config/config.' . APPLICATION_ENV . '.yml'));
});

$app->register(new DoctrineServiceProvider, array(
    "db.options" => $app['config']['database']
));

$app->register(new DoctrineOrmServiceProvider, array(
    "orm.proxies_dir" => __DIR__ . "/../var/doctrine/proxies",
    "orm.auto_generate_proxies" => true,
    "orm.em.options" => array(
        "mappings" => array(
            array(
                "type" => "annotation",
                "namespace" => "Entity",
                "path" => __DIR__ . "/../src/Entity"
            )
        ),
    ),
));
